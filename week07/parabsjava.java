abstract class Man{
    abstract void walk();
    Man(){
        System.out.println(" Man\n");
    }
    public void breathe(){
        System.out.println("Man can breathe\n");
    }
}
class Boy extends Man{
    public void walk(){
        System.out.println("Can Walk");
    }
    Boy(){
        System.out.println("Boy\n");
    }
}
class Main{
    public static void main(String[] args) {
        Boy b = new Boy();
        b.breathe();
        b.walk(); 
    }
}