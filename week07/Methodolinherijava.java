class Base
{
	public int bf(int i)
	{
		System.out.println("Base Output : ");
		return i+3;
	}
}
class Derived extends Base
{
	public double df(double i)
	{
		System.out.println("Derived Output : ");
		return i + 4.9;
	}
}
class Main
{
	public static void main(String args[])
	{
		Derived obj = new Derived();
		System.out.println(obj.bf(3));
		System.out.println(obj.df(5.0));
	}
}