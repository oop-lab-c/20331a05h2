interface Iface
{
    public void bark();
    public void walk();
}
class A implements Iface
{
    public void bark()
    {
        System.out.println("Sound.....\n");
    }
    public void walk()
    {
        System.out.println("Walk.....");
    }
    public static void main(String[] args)
    {
        A a = new A();
        a.walk();
        a.bark();
    }
}