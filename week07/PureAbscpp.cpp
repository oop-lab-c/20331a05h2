#include<iostream>
using namespace std;
class A{
    public:
    virtual void method1(){
        cout<<"method1 for A"<<endl;
    }
    void method2(){
        cout<<"method2 for A"<<endl;
    }
};
class B : public A{
    public:
    void m1(){
        cout<<"method1 in B"<<endl;
    }
    void method2(){
        cout<<"method2 in B"<<endl;
    }
};
int main(int argc, char const *argv[])
{
    A obj;
    B obj1;
    A *a;
    cout<<"Base class pointer to base class object"<<endl;
    a = &obj;
    a->method1();
    a->method2();
    cout<<"\nBase class pointer to derived class object"<<endl;
    a = &obj1;
    a->method1();
    a->method2();
    return 0;
}