class Age{  
    int age(){return 0;}  
    }  
    //Creating child classes.  
    class Father extends Age{  
    int age(){return 54;}  
    }  
    class Mother extends Age{  
    int age(){return 47;}  
    }  
    class Me extends Age{  
    int age(){return 19;}  
    }  
    class Main{  
    public static void main(String args[]){  
    Father f=new Father();  
    Mother m=new Mother();  
    Me me=new Me();  
    System.out.println("Father Age: "+f.age());  
    System.out.println("Mother Age: "+m.age());  
    System.out.println("My Age: "+me.age());  
    }  
    }  
