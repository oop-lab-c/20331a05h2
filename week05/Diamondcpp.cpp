#include<iostream>
using namespace std;
class parent{
    public:
    void show()
    {
        cout<<"i'm parent\n";
    }
};
class child1 : public parent{
    public:
    void see(){
        cout<<"i'm child1\n";
    }
};
class child2 : public parent{
    public:
    void out(){
        cout<<"i'm child2\n";
    }
};
class derived : public child1,public child2{
    public:
    void call(){
        cout<<"i'm derived\n";
    }
};
int main(){
    derived obj;
    obj.see();
    obj.show();
    return 0;
}