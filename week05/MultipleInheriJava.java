
import java.util.Scanner;
class A  
{  
    public void display()  
    {  
    System.out.println("class A  called");  
    }  
}  
class B extends A  
{  
    public void display()  
    {  
        System.out.println("class B  called");  
    }     
}  
class C extends A  
{  
    public void display()  
    {  
        System.out.println("class C  called");  
    }  
}  
  
public class D extends B,C  
{  
    public static void main(String args[])  
    {  
        D d = new D();  
          
        d.display();   
    }  
}