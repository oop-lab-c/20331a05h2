import java.lang.*;
import java.util.Scanner;

class EvenOdd {
    public static void main(String[] args) {
        int a;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a number");
        a = input.nextInt();
        if (a % 2 == 0)
            System.out.println("EVEN");
        else
            System.out.println("ODD");

    }
}